package innopolis.politov;

import java.util.ArrayList;

public class Main {



    public static void main(String[] args) {
        ArrayList<Values> randomValues = new ArrayList<>(100);

        GeneratorThread generatorThread = new GeneratorThread(randomValues);
        Thread threadGenerator = new Thread(generatorThread);
        Thread threadController = new Thread(new ControllerThread(randomValues));
        threadGenerator.start();
        threadController.start();

        try {
            threadController.join();
            generatorThread.setStoped(true);
            threadGenerator.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
