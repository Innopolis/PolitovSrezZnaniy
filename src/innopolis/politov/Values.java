package innopolis.politov;

/**
 * Created by General on 2/13/2017.
 */
public class Values {
    public int val;

    public Values(Integer val) {
        this.val = val;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Values values = (Values) o;
        return val ==  (values.val);
    }

    @Override
    public int hashCode() {
        return val;
    }
}
