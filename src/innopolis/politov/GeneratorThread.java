package innopolis.politov;

import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by General on 2/13/2017.
 */
public class GeneratorThread implements  Runnable {
    private ArrayList<Values> randomValues;

    public void setStoped(boolean stoped) {
        this.stoped = stoped;
    }

    private volatile boolean stoped = false;

    public GeneratorThread(ArrayList<Values> randomValues) {
        this.randomValues = randomValues;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (!stoped) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (randomValues) {
                Integer value = random.nextInt(2);
                randomValues.add(new Values(value));
                System.out.println(value);
                randomValues.notifyAll();
            }
        }

    }
}
