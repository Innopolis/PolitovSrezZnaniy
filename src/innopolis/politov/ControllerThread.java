package innopolis.politov;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by General on 2/13/2017.
 */
public class ControllerThread implements Runnable {
    private ArrayList<Values> randomValues;
    private int currentIndex = 0;
    private HashMap<Values, Integer> unicalValues = new HashMap<>(100);

    public ControllerThread(ArrayList<Values> randomValues) {
        this.randomValues = randomValues;
    }
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (randomValues) {
                for (; currentIndex < randomValues.size();
                     currentIndex++) {
                    Values key = randomValues.get(currentIndex);
                    if (unicalValues.containsKey(key)) {
                        Integer count = unicalValues.get(key);
                        ++count;
                        unicalValues.put(key, count);
                        if (count>=5){
                            System.out.println("найдено 5 повторений одного " +
                                    "числа:" + key.val);
                            return;
                        }
                    } else {
                        unicalValues.put(key, 1);
                    }
                }
                for (Map.Entry<Values, Integer> entry :
                        unicalValues.entrySet()) {
                    System.out.print("<" + entry.getKey().val + ": "
                            + entry.getValue() + "> ");
                }
                System.out.println("");
            }

        }
    }
}
